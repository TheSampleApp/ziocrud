package com.thesampleapp.crud.zio.infra

import com.thesampleapp.crud.zio.domain.{User, UserRepository}
import io.getquill.jdbczio.Quill
import io.getquill.{Escape, H2ZioJdbcContext}
import zio.{Random, Task, ZLayer}

import java.util.UUID
import javax.sql.DataSource
case class UserTable(uuid: UUID, name: String, age: Int){
  def toDomain: User =  User(id = uuid.toString, name = name, age = age)
}
class PersistentUserRepository(ds: DataSource) extends UserRepository{
  val ctx = new H2ZioJdbcContext(Escape)
  import ctx._

  override def save(user: User): Task[String] = {
    for {
      id <- Random.nextUUID
      _ <- ctx.run {
        quote {
          query[UserTable].insertValue {
            lift(UserTable(id, user.name, user.age))
          }
        }
      }
    }yield id.toString
  }.provide(ZLayer.succeed(ds))

  override def findById(id: String): Task[Option[User]] = ctx
    .run {
      quote {
        query[UserTable]
          .filter(p => p.uuid == lift(UUID.fromString(id)))
          .map(u => u.toDomain)
      }
    }
    .provide(ZLayer.succeed(ds))
    .map(_.headOption)

  override def findAll: Task[List[User]] = ctx
    .run {
      quote {
        query[UserTable].map(u => u.toDomain)
      }
    }
    .provide(ZLayer.succeed(ds))

  override def delete(id: String): Task[Unit] = {for{
    _ <- ctx.run{
      quote {
        query[UserTable].filter(u=> u.uuid.toString == id).delete
      }
    }
  }yield ()}.provide(ZLayer.succeed(ds))
}

object PersistentUserRepository {
  def layer: ZLayer[Any, Throwable, PersistentUserRepository] =
    Quill.DataSource.fromPrefix("UserApp") >>>
      ZLayer.fromFunction(new PersistentUserRepository(_))
}