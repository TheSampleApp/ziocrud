package com.thesampleapp.crud.zio.infra

import com.thesampleapp.crud.zio.domain.{User, UserRepository}
import zio.{Random, Ref, Task, UIO, ZLayer}

class InMemoryUserRepository(map: Ref[Map[String, User]]) extends UserRepository{
  override def save(user: User): UIO[String] = {
    for {
        id <- Random.nextUUID.map(_.toString)
        _ <- map.updateAndGet(_ + (id -> user))
    }yield id
  }

  override def findById(id: String): UIO[Option[User]] =
    map.get.map(_.get(id))

  override def findAll: UIO[List[User]] =
    map.get.map(_.values.toList)

  override def delete(id: String): Task[Unit] =
    map.get.map(_.removed(id))
}

object InMemoryUserRepository {
  def layer: ZLayer[Any, Nothing, InMemoryUserRepository] =
    ZLayer.fromZIO(
      Ref.make(Map.empty[String, User]).map(new InMemoryUserRepository(_))
    )
}