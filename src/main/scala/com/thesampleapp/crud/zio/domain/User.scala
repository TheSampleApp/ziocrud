package com.thesampleapp.crud.zio.domain
import zio.json._
import zio.{Task, ZIO}

case class User(id: String, name: String, age: Int)

object User{
  implicit val decoder: JsonDecoder[User] =
    DeriveJsonDecoder.gen[User]
  implicit val encoder: JsonEncoder[User] =
    DeriveJsonEncoder.gen[User]
}

trait UserRepository {
  def save(user: User): Task[String]
  def findById(id: String): Task[Option[User]]
  def findAll: Task[List[User]]
  def delete(id: String): Task[Unit]
}


object UserRepository{
  def save(user: User): ZIO[UserRepository, Throwable, String] =
    ZIO.serviceWithZIO[UserRepository](_.save(user))

  def findById(id: String): ZIO[UserRepository, Throwable, Option[User]] =
    ZIO.serviceWithZIO[UserRepository](_.findById(id))

  def findAll: ZIO[UserRepository, Throwable, List[User]] =
    ZIO.serviceWithZIO[UserRepository](_.findAll)

  def delete(id: String): ZIO[UserRepository, Throwable, Unit] =
    ZIO.serviceWithZIO[UserRepository](_.delete(id))
}