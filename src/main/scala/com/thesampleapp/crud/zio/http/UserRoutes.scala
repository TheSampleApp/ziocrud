package com.thesampleapp.crud.zio.http

import com.thesampleapp.crud.zio.domain.{User, UserRepository}
import zio.ZIO
import zio.http._
import zio.http.model.{Method, Status}
import zio.json._
object UserRoutes {
  def apply(): Http[UserRepository, Nothing, Request, Response] = Http.collectZIO[Request] {
    case Method.GET -> !! / "users" => UserRepository.
      findAll.map(response => Response.json(response.toJson)).orDie

    case Method.GET -> !! / "users"/ userId =>
      UserRepository.findById(userId).map{
        _ match {
          case Some(user) => Response.json(user.toJson)
          case None => Response.status(Status.NotFound)
        }
      }.orDie
    case req @ Method.POST -> !! / "users" => {
      for{
      user <- req.body.asString.map(_.fromJson[User])
      r <- user match{
        case Left(e) =>
          ZIO
            .debug(s"Failed to parse the input: $e")
            .as(
              Response.text(e).setStatus(Status.BadRequest)
            )
        case Right(u) =>
          UserRepository
            .save(u)
            .map(id => Response.text(id))
      }
    } yield r}.orDie

    case Method.DELETE -> !! / "users"/ userId => {
      for{
        _ <- UserRepository.delete(userId)
      }yield ()
    }.map(_ => Response.status(Status.NoContent)).orDie
  }
}
