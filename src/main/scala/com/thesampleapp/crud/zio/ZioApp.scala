package com.thesampleapp.crud.zio

import com.thesampleapp.crud.zio.http.UserRoutes
import com.thesampleapp.crud.zio.infra.InMemoryUserRepository
import zio._
import zio.http._

object ZioApp extends ZIOAppDefault{
  override val run =
    Server.serve(UserRoutes()).provide(Server.default,InMemoryUserRepository.layer)
}
