ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"
val zio_version   = "2.0.10"
lazy val root = (project in file("."))
  .settings(
    name := "ziocrud",
    libraryDependencies += "dev.zio" %% "zio" % zio_version,
    libraryDependencies += "dev.zio" %% "zio-http" % "0.0.5",
    libraryDependencies += "dev.zio" %% "zio-json" % "0.5.0",
    libraryDependencies += "io.getquill"   %% "quill-zio"      % "4.3.0",
    libraryDependencies += "io.getquill"   %% "quill-jdbc-zio" % "4.3.0",
    libraryDependencies += "com.h2database" % "h2"             % "2.1.214"
  )
